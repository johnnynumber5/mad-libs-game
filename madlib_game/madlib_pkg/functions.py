def play_game():
    fname = input('Enter first name of a person: ')
    bodypart = input('Enter a body part: ')
    adjective1 = input('Enter an adjective: ')
    num = input('Enter a number: ')
    adjective2 = input('Enter another adjective: ')
    ptverb1 = input('Enter a past tense verb (ran, walked, etc): ')
    adjective3 = input('Enter another adjective: ')
    verb1 = input('Enter a verb (base form): ')
    noun1 = input('Enter a Noun: ')
    event = input('Enter an event: ')
    adjective4 = input('Enter another adjective')
    ptverb2 = input('Enter another past tense verb: ')
    verb2 = input(
        'Enter a present tense verb + ING (walking, runningm, etc): ')
    adjective5 = input('Enter another adjective: ')
    adjective6 = input('Enter another adjective: ')
    story = "There are times when you find out someone you thought was a friend really isnt. I found out that " + fname + ", who was in my " + bodypart + " my " + adjective1 + " friend, really didn't care about me at all. We had plans on " + num + " occasions where she cancelled at the last minute. She had a grandmother that was very " + adjective2 + " and she was having to help out. I " + ptverb1 + " several times to come and help too but she would say for me to go ahead with our other friends and have a " + \
        adjective3 + " time. She would " + verb1 + " next time. Well last night I went with some friends to the " + noun1 + "event. Of course my bestie was busy helping her " + adjective4 + " granny. Then we " + ptverb2 + \
            " to catch a late movie. I couldn't believe it. There was my bestie " + verb2 + \
        " my boyfriend. ( Who has been grounded for the last month ) I hope they are " + \
        adjective5 + " together. They are a " + adjective6 + " match."
    print(story)

from madlib_pkg import functions
while True:
    print('1) Generate Madlib')
    print('2) Quit')
    choice = input('Please make a selection: ')
    if choice == "1":
        functions.play_game()
    elif choice == "2":
        print('Thanks for playing!  See ya!')
        break
    else:
        print('Invalid selction.  Please try again.')
